\section{Die Platine}

Um den ``Sitzsensor'' möglichst klein und flach verwirklichen zu können, wurde mehrere Platinen bzw.
``Printed-Circuit-Boards'' erstellt und angefertigt. Die Maße der aktuellen Version (V2) betragen
3.1cm auf 2.5cm. Der Li-Ion Akku besitzt ein ähnliche Größe. Die Dicke der Platine beträgt an der
höchsten Stelle ca. 0.5cm. Der Vibrationsmotor befindet sich nicht auf dem PCB, da die Vibration
sonst kaum spürbar wäre.

Die ersten beiden Versionen (V1, V2) der Platine wurden mit dem Tool ``EasyEDA'' erstellt
\url{https://easyeda.com/}.

\begin{figure}[h]
    \centering
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=.5\linewidth]{images/pcb2-top.pdf}
        \caption{Top-Layer der Platine (V2)}
    \end{subfigure}%
    \begin{subfigure}{.5\textwidth}
        \centering
        \includegraphics[width=.5\linewidth]{images/pcb2-bottom.pdf}
        \caption{Bottom-Layer der Platine (V2)}
    \end{subfigure}
\end{figure}

\subsection{Schaltkreis der aktuellen Platine (V2)}

Der Schaltkreis der Platine ist relativ übersichtlich. Man kann ihn in die beiden Teile
Datenerfassung und -verarbeitung und Stromversorgung aufteilen.

\begin{figure}[h]
    \includegraphics[width=16cm]{images/schematic.pdf}
    \centering
    \caption{Schematik des Schaltkreises der Platine}
\end{figure}

\subsubsection{Wichtige Bestandteile des Schaltkreises}

Die Hauptkomponenten der Platine sind folgende:

\begin{itemize}
    \item ATmega328P: \textit{Mikrocontroller} - Auslesen und Verarbeiten der Daten
    \item MPU-6050: \textit{Beschleunigungssensor} - Messen der Rückenneigung
    \item TP4056: \textit{IC für das Laden von Li-Ion Akkus} - Laden des Akkus
    \item MIC5205 \textit{Linearer Spannungs-Regulierer} - Regulierung der Eingangs-Spannung des
    Akkus auf 3.3V
\end{itemize}

\subsubsection{Externe Kontakte und Signale}

Auf der Platine befinden sich einige Lötstellen für das Anschließen weiterer Komponenten:

\begin{itemize}
    \item \textbf{VB1 / VB2} - Kontakte für den Vibrationsmotor
    \item \textbf{BT+ / BT-} - Kontakte für den Li-Ion Akku
    \item \textbf{VIN / GND} - Anschluss für Ladestrom des Akkus und Stromversorgung (soll später
    durch Mikro-USB ersetzt werden)
\end{itemize}

Sechs Kontakte sind auf eine Steckerleiste in der Platine geführt, diese werden für die Programmierung
des ATmega328P benötigt. Sie befinden sich oben rechts in der vorherigen Abbildung der Platine.

\begin{itemize}
    \item \textbf{VCC / GND} - Stromversorgung
    \item \textbf{MOSI / MISO} - Datenübertragungspins ``Master-Out-Slave-In'' und ``Master-In-Slave-Out''
    \item \textbf{SCK} - Serielle Clock
    \item \textbf{RST} - Reset-Pin des ATmega328P
\end{itemize}

\subsubsection{Stromversorgung}

Folgende Angaben sind in dem Datenblatt des ICs zu finden: \cite{tp4056:datasheet}.

Der \textit{TPU4056} ist ein IC (``Integrated Circuit'') der für das korrekte Laden des Li-Ion Akkus
verwendet wird. Der \textbf{TEMP}-Pin kann dazu verwendet werden, den Ladevorgang bei einer zu hohen
Temperatur der Batterie zu unterbrechen. Durch das Verbinden zu GND wird dieses Feature allerdings
nicht verwendet. Durch das Verbinden des \textbf{PROG}-Pins zu GND über einen Widerstand kann der
Ladestrom eingestellt werden. Hierbei gilt folgende Formel (\(V_{PROG} = 1V\)):

\begin{equation}
    I_{BAT} = \frac{V_{PROG}}{R_{PROG}} * 1200
\end{equation}

Durch das Verwenden eines 5k\si{\ohm} Widerstands wird der Ladestrom auf 250mA gesetzt. Während des
Ladens wird der \textbf{CHRG}-Pin auf GND gesetzt, so dass die LED leuchtet, ansonsten befindet er
sich in einem ``High-Impedance-State''. Sobald der Akku geladen ist, wird im IC  der
\textbf{STDBY}-Pin zu GND verbunden, wodurch die andere LED leuchtet, falls die Platine gerade zum
Laden angeschlossen ist. An den \textbf{VCC}-Pin des \textit{TPU4056} wird der Ladestrom
angeschlossen. Durch das Verbinden des \textbf{CE}-Pins (``Chip-Enable'') mit VIN wird dieser
aktiviert. An den \textbf{BAT}-Pin wird der Plus-Pol der Batterie angeschlossen. Die am
\textbf{BAT}-Pin anliegende Spannung wird vom IC auf 4.2V reguliert. Durch den \textbf{GND}-Pin wird
der Chip mit dem gemeinsamen GND des Schaltkreises verbunden.
\cite{tp4056:datasheet}

Durch den Schalter \textit{SW1} kann der Schaltkreis vom Plus-Pol des Akkus getrennt werden, wodurch
der ``Sitzsensor'' ausgeschaltet wird. Der Schalter ist mit dem Eingangspins \textbf{IN} des
\textit{MIC5205} ICs verbunden, welcher von der Firma ``Microchip'' hergestellt wird
(\url{https://www.microchip.com/wwwproducts/en/MIC5205}). Dies ist ein linearer
Spannungs-Regulierer, welche die Eingangsspannung auf konstante 3.3V reguliert. Durch das Verbinden
des \textbf{EN}-Pins mit dem Plus-Pol der Batterie wird der Chip aktiviert. Durch den
\textbf{GND}-Pin wird er mit dem GND des Schaltkreises verbunden. Am \textbf{OUT}-Pin liegen die für
die Schaltung benötigten 3.3V an.
\cite{mic5205:datasheet}

\subsubsection{Mikrocontroller und Sensor}

Folgende Informationen sind dem Datenblatt des ATmega328P entnommen: \cite{atmega:datasheet}.

Der zweite Teil des Schaltkreises besteht aus dem \textit{ATmega328P} und dem \textit{MPU-6050}.
Einige Pins des ATmega328P sind gemäß Datenblatt mit VCC bzw GND verbunden. Der Pin
\textbf{PC6(RESET)} kann über die Steckleiste mit dem Reset-Pin des Programmers verbunden werden. Die
beiden Pins \textbf{PC4(SDA)} und \textbf{PC5(SCL)} stellen die beiden Leitungen für SDA und SCL da
und sind mit denen des MPU-6050 verbunden. Der digitale Pin \textbf{PB0} steuert den
Vibrationsmotor. Die Pins \textbf{PB3(MOSI)}, \textbf{PB4(MISO)} und \textbf{PB5(SCK)} werden für
die Programmierung des Chips benötigt. Die beiden Pins \textbf{PD0(RXD)} und \textbf{PD1(TXD)}
können für serielle Kommunikation via USART verwendet werden, allerdings haben diese Kontakte in der
aktuellen Version des PCB keine herausgeführten Kontaktstellen.

Der \textit{MPU-6050} ist durch die beiden Pins \textbf{SDA} und \textbf{SCL} mit dem ATmega328P
verbunden, wodurch mit I\textsuperscript{2}C Daten ausgetauscht werden können. Die beiden
Widerstände \textit{R5} und \textit{R7} stellen Pull-Up Widerstände für die SCL- und SDA-Leitung da.
Durch das Setzen von Pin \textbf{AD0} auf HIGH kann die I\textsuperscript{2}C-Adresse des Chips von
0x68 auf 0x69 geändert werden. Hier ist dieser Pin allerdings mit GND verbunden, wodurch die Adresse
nicht geändert wird. An \textbf{CLKIN} kann eine externe Clock angeschlossen werden. Da hier die
interne verwendet wird, ist dieser Pin mit GND verbunden. An \textbf{VLOGIC} wird die
Versorgungsspannung für die digitalen I/O-Pins angeschlossen. Im Schaltbild ist dies allerdings mit
GND verbunden. Dies ist ein Fehler in der aktuellen Version (V2) der Platine, welcher in der
nächsten Version (V3) behoben wird. Durch den \textbf{GND} Pin wird der IC mit dem GND des
Schaltkreises verbunden und an \textbf{VDD} wird die Versorgungsspannung für den Chip angeschlossen.
\textbf{FSYNC} kann für die Synchronisation der Kommunikation bei Verlust von einzelnen Bits
verwendet werden. Hier ist er mit GND verbunden, wodurch dies deaktiviert ist. Der an \textbf{CPOUT}
angeschlossene Kondensator ist Teil einer sogenannten ``Charge-Pump'', welche verwendet werden kann
um Spannungen zu erhöhen oder zu erniedrigen. An den \textbf{REGOUT}-Pin wird ebenfalls ein
Kondensator angeschlossen. Dieser dient als Filter. \cite{mpu:datasheet}

\subsubsection{Weitere Bauteile}

Die folgenden Informationen sind der Produktseite des Vibrationsmotors entnommen:
\url{https://www.amazon.de/Brunswick-0-05A-M%C3%BCnze-Handy-Vibration/dp/B00PZYMCT8/ref=sr_1_5?keywords=3V+vibrationsmotor&qid=1572484809&sr=8-5}

Der 3V Vibrationsmotor vom Hersteller ``Brunswick'' wird durch einen Transistor gesteuert. Dies ist
nötig, da der Motor bis zu 100mA benötigen kann \cite{amazon:vib}, durch den GPIO-Pi des ATmega328Ps
allerdings nur maximal 20mA fließen dürfen. Die meisten weiteren Bauteile auf der Platine sind
Kondensatoren zur Spannungsstabilisierung.

\subsection{Unterschiede von Version 2 zu Version 1}

Im Vergleich zu der ersten Version der Platine hat sich nur das Layout, wenige Bauteile und die
externen Kontakte geändert. Während bei V1 noch die TX/RX Pins des ATmega328P zu Kontakten geführt
wurden, sind es nun MOSI, MISO und SCK. Diese sind für die Programmierung des ATmega328P
notwendig, da Die Programmierung über UART (TX/RX) nur möglich ist, falls der Arduino-Bootloader
bereits auf den Chip gebrannt wäre. Ebenfalls wurde der RESET-Knopf entfernt, da dies automatisch
über den RST-Pin geschieht. Durch das Verschieben einiger Bauteile wurde die Größe der Platine
weiter minimiert.

\subsection{Pläne für Version 3}

Wie bereits erwähnt ist in der aktuellen Schaltung der \textbf{VLOGIC}-Pin des \textit{MPU-6050} mit
\textbf{GND} verbunden, obwohl er mit \textbf{VCC} (3.3V) verbunden sein müsste. Ebenfalls werden
die Kondensatoren, welche für eine konstante Spannungsversorgung der Bauteile sorgen, näher an deren
\textbf{GND} und \textbf{VCC} Pins positioniert. Auch soll ein Mikro-USB-Anschluss hinzugefügt
werden, über den der Akku leichter geladen werden kann.

\begin{figure}[h]
    \includegraphics[width=16cm]{images/platine-com.jpg}
    \centering
    \caption{Platine ohne/mit aufgelöteten Komponenten (V2)}
\end{figure}
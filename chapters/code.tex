\label{sec:code}
\section{Programmierung}

Das Programm für den ``Sitzsensor'' ist in der Programmiersprache C geschrieben. Es setzt die
benötigten Register des ATmega328P, um ihn mit I\textsuperscript{2}C auszulesen und wertet die Daten
aus, um anschließend den Vibrationsmotor zu aktivieren, falls der Anwender schief oder zu lange
sitzt.

\subsection{Struktur des Programms}

Das Programm ist in mehrere Dateien aufgeteilt:

\begin{itemize}
    \item \verb|main.c| Einstiegspunkt des Programms
    \item \verb|i2c.c / i2c.h| I\textsuperscript{2}C-Library
    \item \verb|USART.c / USART.h| USART-Library (Debugging)
    \item \verb|pinDefines.h| Konstanten für die Pinbelegung
\end{itemize}

Der zentrale Einstiegspunkt des Programms ist die Funktion \verb|int main(void)| in der Datei
\verb|main.c|. Zuvor werden einige Konstanten definiert, in denen die Adressen des MPU-6050 und die
der benötigten Register gespeichert werden.

Anschließend wird eine Variable definiert, in der später der gemessene Wert des
Beschleunigungssensors gespeichert wird. Ebenfalls wird der digitale Pin \textbf{PB0} als Ausgang
definiert. An diesem ist der Vibrationsmotor (bzw. Transistor-Basis) angeschlossen.

Nun werden die I\textsuperscript{2}C- als auch die USART-Schnittstellen aktiviert. Da mit
USART serielle Nachrichten an einen Computer gesendet werden können, eignet sich dies gut für
Entwicklungs- und Debugging-Zwecke.

Vor der Haupt-Schleife wird der MPU-6050 noch initialisiert.\newline Hierfür wird 0x00 an das
``PWRMGMT1'' Register gesendet. Anschließend wird durch die Funktion \verb|calculate_default(10)|
ein Normal-Wert bzw. eine ``Normal-Neigung'' des Rückens berechnet. Diese Kalibrierung dient später
für die Erkennung, ob man schief sitzt. Vor und nach der Kalibrierung wird ein kurzes Muster durch
den Vibrationsmotor abgespielt. Durch die \verb|printString()| Funktion wird ein String via USART an
eine angeschlossene serielle Schnittstelle (z.B. serielle Konsole) gesendet.

\subsubsection{Die Haupt-Schleife}

Der Code innerhalb der \verb|while (1) {}| Schleife wird endlos ausgeführt. Dies ist vergleichbar
mit der \verb|void loop()| Funktion bei der Programmierung eines Arduinos.
Zunächst wird der aktuelle Register Wert mit \verb|read_reg_value()| ausgelesen. Dies ist eine
kleine Hilfs-Funktion, welche über I\textsuperscript{2}C den Wert des Registers für die X-Achse des
Beschleunigungssensors ausliest. Dieser wird nun mit dem zuvor kalibrierten Normal-Wert verglichen
und bei einer Überschreitung der Vibrationsmotor aktiviert.

\subsection{Die I\textsuperscript{2}C-Library}

In dem Buch ``Make: AVR Programming'' \cite{avr:programming} wird die Funktionsweise von
I\textsuperscript{2}C sehr genau erklärt. Ebenfalls wird eine einfache Library für die Verwendung
des Protokolls mit Hilfe eines ATmega168 beschrieben. Diese kann ebenfalls mit einem ATmega328P
verwendet werden. Sie stellt eine Schnittstelle zwischen den Registern des Mikrocontrollers und dem
weiteren Code da.

\subsubsection{Initialisierung der I\textsuperscript{2}C-Schnittstelle}

Um mit der Übertragung von Daten zu beginnen, müssen zunächst einige Einstellungen vorgenommen
werden. Dies geschieht durch das Setzen von einzelnen Bits in Registern des ATmega328P. Durch den
Befehl \verb|TWBR = 32;| wird die Frequenz des SCL-Signals auf ca. 100kHz gesetzt. Dies lässt sich durch
folgende Formel berechnen, welche auf S. 222 im Datenblatt zu finden ist. \cite{atmega:datasheet}

\begin{equation}
    \text{SCL-Frequency} = \frac{\text{CPU Clock Frequency}}{16 + 2 * \text{TWBR} * \text{PrescalerValue}}
\end{equation}

Die Frequenz ist durch das Verwenden der internen Clock auf 8MHz festgelegt. Die ``PrescalerValue''
ist ein einstellbarer Wert, durch den die Frequenz künstlich herunterskaliert werden kann. Bei
einem Wert von 8 wäre die Frequenz nur noch 1MHz.\newline
Durch den Befehl \verb|clock_prescale_set(clock_div_1);| wird dieser auf 1 gesetzt, wodurch die
Frequenz bei 8MHz bleibt. Der Befehl \verb+TWCR |= (1 << TWEN);+ aktiviert die
I\textsuperscript{2}C-Schnittstelle (bzw. Two-Wire-Interface).

\subsubsection{Senden von Daten}

Um eine Nachricht auf den Bus (SDA) zu schreiben, müssen wieder Bits im TWCR-Register gesetzt
werden. Dies geschieht über folgende Befehle, wodurch ein Start-Signal auf SDA
geschrieben wird, um den Anfang einer neuen Übertragung zu signalisieren. Anschließend wartet der
Prozessor bis die ``Start-Condition'' übertragen wurde.
\begin{lstlisting}[language=C]
void i2cStart(void) {
    TWCR = (_BV(TWINT) | _BV(TWEN) | _BV(TWSTA));
    i2cWaitForComplete();
}
\end{lstlisting}
Nun kann die Adresse des Komponenten, mit dem der Master kommunizieren möchte, auf SDA geschrieben
werden. Dies geschieht durch folgenden Code:
\begin{lstlisting}[language=C]
void i2cSend(uint8_t data) {
    TWDR = data;
    TWCR = (_BV(TWINT) | _BV(TWEN));
    i2cWaitForComplete();
}
\end{lstlisting}
Zunächst wird das zu sendende Byte in das Daten-Register TWDR geschrieben. Durch das Setzen von den
TWINT und TWEN Bits, werden die Daten übertragen. Anschließend wartet der Prozessor bis die Daten
übertragen und vom Slave angenommen wurden (Acknowledge-Bit gesetzt).

\subsubsection{Empfangen von Daten}

Um Daten von Slaves zu empfangen, wie zum Beispiel die Werte des Beschleunigungssensors, müssen
ebenfalls Bits im TWCR-Register gesetzt werden.
\begin{lstlisting}[language=C]
uint8_t i2cReadNoAck(void) {
    TWCR = (_BV(TWINT) | _BV(TWEN));
    i2cWaitForComplete();
    return (TWDR);
}
\end{lstlisting}
Nach dem Warten wird der Inhalt des TWDR-Registers zurückgegeben. Dieses beinhaltet bei einer
Lese-Operation die gelesenen Bytes und bei einer Sende-Operation die zu sendenden Bytes.

\subsubsection{Beenden einer Übertragung}

Um eine I\textsuperscript{2}C-Übertragung zu beenden muss ein Stop-Signal generiert werden. Dies
wird von der folgenden Funktion übernommen:
\begin{lstlisting}[language=C]
void i2cStop(void) {
    TWCR = (_BV(TWINT) | _BV(TWEN) | _BV(TWSTO));
}
\end{lstlisting}

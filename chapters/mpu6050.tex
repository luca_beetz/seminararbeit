\section{Der MPU-6050 (Beschleunigungssensor)}

Um die Krümmung des Rückens zu messen, wird der ``MPU-6050 Six-Axis'' Sensor von ``InvenSense''
verwendet \newline(\url{https://www.invensense.com/products/motion-tracking/6-axis/mpu-6050/}). Dieser
enthält ein Gyroskop, als auch einen Beschleunigungssensor. Für die Messung der Neigung des Rückens
wird allerdings nur letzterer benötigt. Die gemessenen Daten werden in internen Registern des
MPU-6050 zum Auslesen bereitgestellt. Die folgenden technischen Informationen sind dem Datenblatt
entnommen: \cite{mpu:datasheet}

\subsection{Technische Details}

Der Sensor kann mit einer Spannung von 2.375V bis 3.46V betrieben werden und unterstützt nur
I\textsuperscript{2}C als serielle Schnittstelle. Der Beschleunigungssensor misst die Beschleunigung
in drei Achsen. Hierbei wird die Beschleunigung durch die Gravitation der Erde nicht
herausgerechnet, wodurch es möglich ist, die Neigung des Sensors im Raum zu bestimmen. Der IC
befindet sich in einem QFN-Package mit der Größe von 4x4x0.9mm. Bei Verwendung beider, im Chip
verbauten, Sensoren benötigt er 3.8mA.

\subsection{Kommunikation mit dem Sensor via I\textsuperscript{2}C}

Die Kommunikation zwischen Mikrochip (ATmega328P) und Sensor (MPU-6050) findet über das
I\textsuperscript{2}C-Protokoll statt. Dieses besteht aus zwei Signalen. Einem Clock-Signal (SCL),
welches die Taktrate der Übertragung vorgibt, und einem Daten-Signal (SDA), welches für die
Übertragung der eigentlichen Daten verwendet wird.

\label{sec:i2c}
\subsection{Übersicht über I\textsuperscript{2}C}

Das ``Inter-integrated Circuit Protocol'' wird für die Kommunikation zwischen mehreren ``Slaves''
mit einem (oder mehreren) ``Master'' verwendet. Der MPU-6050 funktioniert immer als Slave, während
der ATmega328P in dieser Schaltung als Master agiert. Innerhalb eines I\textsuperscript{2}C-Bus
können standardweise 112 verschiedene Geräte verwendet werden, wobei jedes eine eindeutige 7-bit
Adresse haben muss. Eine mögliche Erweiterung auf 1024 Geräte mit Hilfe von 10-bit Adressen ist in
folgendem Dokument beschrieben. \cite{i2c:bus-spec} Die Adresse des Sensors ist 0x68, welche durch
das Setzen des Pins 'A0' auf HIGH zu 0x69 geändert werden kann. So könnten zum Beispiel zwei oder
mehrere Sensoren an einen I\textsuperscript{2}C-Bus angeschlossen werden, indem durch eine weitere
technische Schaltung immer nur die Adresse eines Chips 0x68 ist, die der anderen allerdings auf 0x69
geändert wird.

\subsubsection{Eine I\textsuperscript{2}C-Nachricht}

Nachrichten bestehen aus zwei Teilen: Einem Adress-Paket und mindestens einem Daten-Packet. Das
Adress-Packet beinhaltet die Adresse des Slaves mit dem der Master kommunizieren möchte als auch die
Information, ob es sich um eine Schreib- (8. Bit := 0) bzw. Leseoperation (8.Bit := 1) handelt. Die
Daten-Pakete enthalten je ein Byte an Daten. Jedes Paket wird durch den Empfänger mit einem
``Acknowledge-Bit'' bestätigt, indem dieser SDA auf LOW zieht. Eine genaue Beschreibung des
I\textsuperscript{2}C-Protokolls befindet sich hier: \cite{i2c:description}

\begin{figure}[h]
    \includegraphics[width=16cm]{images/i2c-message.png}
    \centering
    \caption{Diagramm einer einzelnen I\textsuperscript{2}C Übertragung \cite{i2c:description}}
\end{figure}

Auch kann man Start- bzw. Stop-Signale erkennen. Diese werden verwendet, um den Anfang und
das Ende einer I\textsuperscript{2}C Datenübertragung zu kennzeichnen. Bei einem Start-Signal
wechselt SDA auf LOW während SCL HIGH ist. Bei einem Stop-Signal wechselt SDA auf HIGH während SCL
HIGH ist. Zwischen diesen Signalen wechselt der Wert von SDA nur bei SCL gleich LOW.

\subsection{Register}

Der MPU-6050 enthält interne 8-bit Register, welche zum Speichern von Einstellungen als auch zur
Bereitstellung der Sensorwerte verwendet werden. Alle haben eine 8-bit Registeradresse, anhand
welcher sie beschrieben bzw. ausgelesen werden können. Die folgenden Informationen sind im
Register-Datenblatt zu finden: \cite{mpu:register}

\subsubsection{Einstellungsregister}

Um mit dem Sensor zu kommunizieren und dessen Daten abzurufen, müssen zunächst zwei Register, welche
einige benötigte Parameter enthalten, beschrieben werden. An die beiden ``Power Management''
Register 0x6B und 0x6C wird je 0x00 gesendet, wodurch der Sensor auf Standardeinstellungen gesetzt und
alle Sensoren aktiviert werden. \cite{mpu:register}

\begin{figure}[h]
    \includegraphics[width=16cm]{images/mpu-register-107.png}
    \centering
    \caption{Register 0x6B für das Setzen einiger Grundeinstellungen \cite{mpu:register}}
\end{figure}

\begin{figure}[h]
    \includegraphics[width=16cm]{images/mpu-register-108.png}
    \centering
    \caption{Register 0x6C für das Aktivieren der Sensoren \cite{mpu:register}}
\end{figure}

\newpage
\subsubsection{Datenregister}

Der MPU-6050 stellt die gemessenen Daten in internen 8-bit Registern zum Auslesen über
I\textsuperscript{2}C bereit. Die Messwerte des Beschleunigungssensors befinden sich in den
Registern 0x3B bis 0x40. Da jeder Wert aus einer 16-bit Zahl besteht, werden je zwei Register
benötigt.

\begin{figure}[h]
    \includegraphics[width=16cm]{images/mpu-register-59-to-64.png}
    \centering
    \caption{Register für die Beschleunigungswerte \cite{mpu:register}}
\end{figure}